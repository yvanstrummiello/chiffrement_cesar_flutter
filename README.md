# chiffrement de César
## Défi de formation au développement Flutter - 1ère application

Défi de chiffrement César. Pour ce défi, le décalage de caractère utilise la table ASCII.

La clé de chiffrage est choisie par l'utilisateur.

Une fonction vérifie l'intégrité du chiffrage en affichant le message d'origine, ce qui permet à l'utilisateur de contrôler le bon fonctionnement du chiffrage

