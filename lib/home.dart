import 'package:chiffrement/message.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _key = 0;
  String _message = "";
  late bool messageExist = false;
  late String _encryptedMessage;
  late String _decryptedMessage;

  _savedKey(value) {
    _key = int.parse(value);
  }

  _savedMessage(String value) {
    _message = value;
  }

  _cryptMessage(String message, int key) {
    Message encryptedMessage = Message(message, key);
    _encryptedMessage = encryptedMessage.encodeMessageToASCII();
    _decryptMessage(_key, _encryptedMessage);
    setState(() {
      messageExist = true;
    });
  }

  _decryptMessage(int key, String message) {
    Message decryptedMessage = Message(message, key);
    _decryptedMessage = decryptedMessage.decodeMessageFromASCII();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text("Chiffrement de César"),
          centerTitle: true,
        ),
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: Text(
                  "Message à chiffrer :",
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(left: 20.0, right: 20.0, top: 8.0),
                child: TextField(
                  autocorrect: false,
                  onChanged: _savedMessage,
                  decoration: const InputDecoration(
                    hintText: "Saisissez votre message...",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5.0))),
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 12.0),
                child: Text(
                  "Choisir la clé de chiffrement :",
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 5.0, left: 160.0, right: 160.0),
                      child: TextField(
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly,
                        ],
                        onChanged: _savedKey,
                        maxLength: 1,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0))),
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: ElevatedButton(
                  onPressed: () => _cryptMessage(_message, _key),
                  child: const Text("Chiffrer le message"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 12.0),
                child: Image.asset(
                  'assets/julius.png',
                  height: 100.0,
                ),
              ),
              if (messageExist == true)
                const Padding(
                  padding: EdgeInsets.only(top: 12.0),
                  child: Text(
                    "Message chiffré :",
                  ),
                ),
              if (messageExist == true)
                Padding(
                  padding:
                      const EdgeInsets.only(left: 20.0, right: 20.0, top: 8.0),
                  child: TextFormField(
                    initialValue: _encryptedMessage,
                    enabled: false,
                    enableInteractiveSelection: true,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                    ),
                  ),
                ),
              if (messageExist == true)
                const Padding(
                  padding: EdgeInsets.only(top: 12.0),
                  child: Text(
                    "Contrôle du message d'origine :",
                  ),
                ),
              if (messageExist == true)
                Padding(
                  padding:
                      const EdgeInsets.only(top: 8.0, left: 20.0, right: 20.0),
                  child: TextFormField(
                    initialValue: _decryptedMessage,
                    enabled: false,
                    enableInteractiveSelection: true,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                    ),
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
}
