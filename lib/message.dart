class Message {
  String _message;
  int _key = 0;
  final _charMinTableASCII = 32;
  final _charMaxTableASCII = 127;
  final _junmpCharASCII = 95;
  final List<int> _convertMesssageASCII = [];
  final List<int> _convertASCIItoMessage = [];
  final List<int> _encodedMessageASCII = [];
  final List<int> _decodedASCIIMessage = [];

  String get message => _message;

  set message(String message) {
    _message = message;
  }

  Message(this._message, this._key);

  String encodeMessageToASCII() {
    String treatedMessage = "";
    _transformMessageToASCII();
      for (var transformedMessage in _encodedMessageASCII) {
        treatedMessage += String.fromCharCode(transformedMessage);
      }
    return treatedMessage;
  }

  String decodeMessageFromASCII() {
    String decodedMessage = "";
    _transformASCIIToMessage();
    for (var transformedMessage in _decodedASCIIMessage) {
      decodedMessage += String.fromCharCode(transformedMessage);
    }
    return decodedMessage;
  }

  _transformMessageToASCII() {
    //Transformer lettre en code ASCII
    for (int letter = 0; letter < _message.length; letter++) {
      _convertMesssageASCII.add(_message.codeUnitAt(letter));
    }
    //Application du décalage selon clé choisie
    for (var codeASCII in _convertMesssageASCII) {
      if (codeASCII < _charMinTableASCII + _key) {
        _encodedMessageASCII.add(codeASCII - _key + _junmpCharASCII);
      } else {
        _encodedMessageASCII.add(codeASCII - _key);
      }
    }
  }

  _transformASCIIToMessage(){
    //Transformer lettre en code ASCII
    for (int letter = 0; letter < _message.length; letter++) {
      _convertASCIItoMessage.add(_message.codeUnitAt(letter));
    }
    //Application du décalage inverse pour décoder le message original
    for (var codeASCII in _convertASCIItoMessage) {
      if (codeASCII >= _charMaxTableASCII - _key){
        _decodedASCIIMessage.add(codeASCII + _key - _junmpCharASCII);
      } else {
        _decodedASCIIMessage.add(codeASCII + _key);
      }
    }
  }
}
